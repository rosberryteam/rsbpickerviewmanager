# RSBPickerViewManager

[![CI Status](http://img.shields.io/travis/Anton Kormakov/RSBPickerViewManager.svg?style=flat)](https://travis-ci.org/Anton Kormakov/RSBPickerViewManager)
[![Version](https://img.shields.io/cocoapods/v/RSBPickerViewManager.svg?style=flat)](http://cocoapods.org/pods/RSBPickerViewManager)
[![License](https://img.shields.io/cocoapods/l/RSBPickerViewManager.svg?style=flat)](http://cocoapods.org/pods/RSBPickerViewManager)
[![Platform](https://img.shields.io/cocoapods/p/RSBPickerViewManager.svg?style=flat)](http://cocoapods.org/pods/RSBPickerViewManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RSBPickerViewManager is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RSBPickerViewManager"
```

## Author

Anton Kormakov, anton.kormakov@rosberry.com

## License

RSBPickerViewManager is available under the MIT license. See the LICENSE file for more info.
