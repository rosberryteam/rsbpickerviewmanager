//
//  main.m
//  RSBPickerViewManager
//
//  Created by Anton Kormakov on 05/24/2016.
//  Copyright (c) 2016 Anton Kormakov. All rights reserved.
//

@import UIKit;
#import "RSBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RSBAppDelegate class]));
    }
}
