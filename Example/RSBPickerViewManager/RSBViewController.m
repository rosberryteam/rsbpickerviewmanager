//
//  RSBViewController.m
//  RSBPickerViewManager
//
//  Created by Anton Kormakov on 05/24/2016.
//  Copyright (c) 2016 Anton Kormakov. All rights reserved.
//

#import "RSBViewController.h"

#import <RSBPickerViewManager/RSBPickerViewManager.h>
#import <RSBPickerViewManager/RSBPickerViewManagerTextAdaptor.h>
#import <RSBPickerViewManager/RSBPickerViewManagerViewAdaptor.h>

@interface RSBViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (nonatomic, strong) RSBPickerViewManager<RSBPickerViewManagerViewAdaptor *> *manager;

@end

@implementation RSBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
    NSMutableArray *items = [NSMutableArray array];
    for (long i = 1; i < 32; ++i) {
        NSString *title = [NSString stringWithFormat:@"Item %ld", i];
        
        RSBPickerViewCellItem *item = [[RSBPickerViewCellItem alloc] init];
        item.title = title;
        item.itemDidSelectBlock = ^(UIPickerView *pickerView, NSIndexPath *indexPath) {
            NSLog(@"Selected: %@", title);
            NSLog(@"%@", [[self.manager selectedItems] valueForKey:@"title"]);
        };
        [items addObject:item];
    }
    
    RSBPickerViewComponentItem *component = [[RSBPickerViewComponentItem alloc] initWithItems:[items copy]];
    
    self.manager = [[RSBPickerViewManager alloc] initWithPickerView:self.pickerView adaptor:[[RSBPickerViewManagerViewAdaptor alloc] init]];
    self.manager.componentItems = @[component];
    
    [self.manager selectItem:[component.cellItems lastObject] animated:YES];
}

@end
