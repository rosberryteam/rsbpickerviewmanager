//
//  RSBAppDelegate.h
//  RSBPickerViewManager
//
//  Created by Anton Kormakov on 05/24/2016.
//  Copyright (c) 2016 Anton Kormakov. All rights reserved.
//

@import UIKit;

@interface RSBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
