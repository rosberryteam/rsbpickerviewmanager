//
//  ViewTests.m
//  RSBPickerViewManager
//
//  Created by Anton K on 7/26/17.
//  Copyright © 2017 Anton Kormakov. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <RSBPickerViewManager/RSBPickerViewManager.h>
#import <RSBPickerViewManager/RSBPickerViewManagerViewAdaptor.h>

@interface MockViewCellItem : RSBPickerViewCellItem

@property (nonatomic, readonly) BOOL didCallViewDataSourceMethod;

@end

@implementation MockViewCellItem

- (UIView *)viewForRowInPickerView:(UIPickerView *)pickerView reusingView:(UIView *)view {
    _didCallViewDataSourceMethod = YES;
    return [super viewForRowInPickerView:pickerView reusingView:view];
}

@end

@interface ViewTests : XCTestCase

@property (nonatomic) RSBPickerViewManager *manager;
@property (nonatomic) MockViewCellItem *item;

@end

@implementation ViewTests

- (void)setUp {
    [super setUp];
    
    self.item = [[MockViewCellItem alloc] init];
    RSBPickerViewComponentItem *component = [[RSBPickerViewComponentItem alloc] initWithItems:@[self.item]];
    
    self.manager = [[RSBPickerViewManager alloc] initWithAdaptor:[[RSBPickerViewManagerViewAdaptor alloc] init]];
    
    self.manager.pickerView.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:self.manager.pickerView];
    self.manager.componentItems = @[component];
    [self.manager.pickerView layoutIfNeeded]; // force to fetch data
}

- (void)tearDown {
    [super tearDown];
    
    [self.manager.pickerView removeFromSuperview];
    self.manager = nil;
}

- (void)testDataSourceCalls {
    XCTAssert(self.item.didCallViewDataSourceMethod);
}
@end
