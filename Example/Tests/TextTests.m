//
//  TextTests.m
//  RSBPickerViewManager
//
//  Created by Anton K on 7/26/17.
//  Copyright © 2017 Anton Kormakov. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <RSBPickerViewManager/RSBPickerViewManager.h>
#import <RSBPickerViewManager/RSBPickerViewManagerTextAdaptor.h>

@interface MockTextCellItem : RSBPickerViewCellItem

@property (nonatomic, readonly) BOOL didCallTitleDataSourceMethod;
@property (nonatomic, readonly) BOOL didCallAttributedTitleDataSourceMethod;

@end

@implementation MockTextCellItem

- (NSString *)titleForRowInPickerView:(UIPickerView *)pickerView {
    _didCallTitleDataSourceMethod = YES;
    return [super titleForRowInPickerView:pickerView];
}

- (NSAttributedString *)attributedTitleForRowInPickerView:(UIPickerView *)pickerView {
    _didCallAttributedTitleDataSourceMethod = YES;
    return [super attributedTitleForRowInPickerView:pickerView];
}

@end

@interface TextTests : XCTestCase

@property (nonatomic) RSBPickerViewManager *manager;
@property (nonatomic) MockTextCellItem *item;

@end

@implementation TextTests

- (void)setUp {
    [super setUp];
    
    self.item = [[MockTextCellItem alloc] init];
    RSBPickerViewComponentItem *component = [[RSBPickerViewComponentItem alloc] initWithItems:@[self.item]];
    
    self.manager = [[RSBPickerViewManager alloc] initWithAdaptor:[[RSBPickerViewManagerTextAdaptor alloc] init]];
    
    self.manager.pickerView.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:self.manager.pickerView];
    self.manager.componentItems = @[component];
    [self.manager.pickerView layoutIfNeeded]; // force to fetch data
}

- (void)tearDown {
    [super tearDown];
    
    [self.manager.pickerView removeFromSuperview];
    self.manager = nil;
}

- (void)testDataSourceCalls {
    XCTAssert(self.item.didCallAttributedTitleDataSourceMethod);
    XCTAssert(self.item.didCallTitleDataSourceMethod);
}

@end
