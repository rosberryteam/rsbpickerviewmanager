//
//  RSBPickerViewManagerTests.m
//  RSBPickerViewManagerTests
//
//  Created by Anton Kormakov on 05/24/2016.
//  Copyright (c) 2016 Anton Kormakov. All rights reserved.
//

@import XCTest;

#import <RSBPickerViewManager/RSBPickerViewManager.h>
#import <RSBPickerViewManager/RSBPickerViewManagerTextAdaptor.h>

@interface MockPickerView : UIPickerView

@property (nonatomic, readonly) BOOL didCallReloadAllComponentsMethod;
@property (nonatomic, readonly) NSInteger reloadComponentMethodIndex;

@end

@implementation MockPickerView

- (void)reloadAllComponents {
    _didCallReloadAllComponentsMethod = YES;
    [super reloadAllComponents];
}

- (void)reloadComponent:(NSInteger)component {
    _reloadComponentMethodIndex = component;
    [super reloadComponent:component];
}

- (void)simulateItemSelection:(NSIndexPath *)indexPath {
    [self selectRow:indexPath.row inComponent:indexPath.section animated:NO];
    [self.delegate pickerView:self didSelectRow:indexPath.row inComponent:indexPath.section];
}

@end

@interface CommonTests : XCTestCase

@property (nonatomic) MockPickerView *pickerView;
@property (nonatomic) RSBPickerViewManager *manager;
@property (nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation CommonTests

- (void)setUp {
    [super setUp];
    
    NSMutableArray *components = [NSMutableArray array];
    for (int i = 0; i < 3; ++i) {
        NSMutableArray *items = [NSMutableArray array];
        for (int i = 0; i < 3; ++i) {
            RSBPickerViewCellItem *item = [[RSBPickerViewCellItem alloc] init];
            item.itemDidSelectBlock = ^(UIPickerView *pickerView, NSIndexPath *indexPath) {
                self.selectedIndexPath = indexPath;
            };
            [items addObject:item];
        }
        
        RSBPickerViewComponentItem *component = [[RSBPickerViewComponentItem alloc] initWithItems:[items copy]];
        [components addObject:component];
    }
    
    self.pickerView = [[MockPickerView alloc] init];
    self.manager = [[RSBPickerViewManager alloc] initWithPickerView:self.pickerView adaptor:[[RSBPickerViewManagerTextAdaptor alloc] init]];
    self.manager.componentItems = [components copy];
}

- (void)tearDown {
    [super tearDown];
    self.manager = nil;
}

- (void)testComponentsMapping {
    XCTAssert([self.manager.componentItems count] == self.manager.pickerView.numberOfComponents);
}

- (void)testItemsMapping {
    XCTAssert(self.manager.componentItems.firstObject.cellItems.count == [self.manager.pickerView numberOfRowsInComponent:0]);
}

- (void)testDataReloading {
    XCTAssert(self.pickerView.didCallReloadAllComponentsMethod);
    
    [self.manager reloadComponent:self.manager.componentItems.lastObject];
    XCTAssert(self.pickerView.reloadComponentMethodIndex == (self.manager.componentItems.count - 1));
}

- (void)testItemSelection {
    id<RSBPickerViewCellItemProtocol> firstItem = self.manager.componentItems.firstObject.cellItems.firstObject;
    XCTAssert([self.manager selectedItemInComponent:self.manager.componentItems.firstObject] == firstItem);
    
    id<RSBPickerViewCellItemProtocol> lastItem = self.manager.componentItems.firstObject.cellItems.lastObject;
    [self.manager selectItem:lastItem animated:NO];
    XCTAssert([self.manager selectedItemInComponent:self.manager.componentItems.firstObject] == lastItem);
    XCTAssert([self.manager.selectedItems containsObject:lastItem]);
    
    NSIndexPath *expectedIndexPath = [NSIndexPath indexPathForRow:(self.manager.componentItems.firstObject.cellItems.count - 1) inSection:0];
    [self.pickerView simulateItemSelection:expectedIndexPath];
    XCTAssert([self.selectedIndexPath isEqual:expectedIndexPath]);
}

- (void)testItemSelectionFailScenarios {
    XCTAssert([self.manager selectedItemInComponent:nil] == nil);
    
    NSArray<id<RSBPickerViewCellItemProtocol>> *selectedItems = [self.manager selectedItems];
    
    [self.manager selectItem:[[RSBPickerViewCellItem alloc] init] inComponent:self.manager.componentItems.firstObject animated:NO];
    XCTAssert([[self.manager selectedItems] isEqual:selectedItems]);
    
    [self.manager selectItem:[[RSBPickerViewCellItem alloc] init] inComponent:[[RSBPickerViewComponentItem alloc] init] animated:NO];
    XCTAssert([[self.manager selectedItems] isEqual:selectedItems]);
}

@end

