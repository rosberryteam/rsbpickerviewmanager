//
//  ComponentTests.m
//  RSBPickerViewManager
//
//  Created by Anton K on 7/26/17.
//  Copyright © 2017 Anton Kormakov. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <RSBPickerViewManager/RSBPickerViewManager.h>
#import <RSBPickerViewManager/RSBPickerViewManagerTextAdaptor.h>

@interface MockComponentItem : RSBPickerViewComponentItem

@property (nonatomic, readonly) BOOL didCallWidthDataSourceMethod;
@property (nonatomic, readonly) BOOL didCallHeightDataSourceMethod;

@end

@implementation MockComponentItem

- (CGFloat)widthForComponentInPickerView:(UIPickerView *)pickerView {
    _didCallWidthDataSourceMethod = YES;
    return [super widthForComponentInPickerView:pickerView];
}

- (CGFloat)heightForRowInPickerView:(UIPickerView *)pickerView {
    _didCallHeightDataSourceMethod = YES;
    return [super heightForRowInPickerView:pickerView];
}

@end

@interface ComponentTests : XCTestCase

@property (nonatomic) RSBPickerViewManager *manager;
@property (nonatomic) MockComponentItem *component;

@end

@implementation ComponentTests

- (void)setUp {
    [super setUp];
    
    RSBPickerViewCellItem *item = [[RSBPickerViewCellItem alloc] init];
    self.component = [[MockComponentItem alloc] initWithItems:@[item]];
    
    self.manager = [[RSBPickerViewManager alloc] initWithAdaptor:[[RSBPickerViewManagerTextAdaptor alloc] init]];
    
    self.manager.pickerView.frame = [UIScreen mainScreen].bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:self.manager.pickerView];
    self.manager.componentItems = @[self.component];
    [self.manager.pickerView layoutIfNeeded]; // force to fetch data
}

- (void)tearDown {
    [super tearDown];
    
    [self.manager.pickerView removeFromSuperview];
    self.manager = nil;
}

- (void)testInitializer {
    XCTAssert(self.component.cellItems.count != 0);
}

- (void)testDataSourceCalls {
    XCTAssert(self.component.didCallWidthDataSourceMethod);
    XCTAssert(self.component.didCallHeightDataSourceMethod);
}

@end
