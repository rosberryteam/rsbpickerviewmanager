//
//  RSBPickerViewCellItem.m
//  Pods
//
//  Created by Anton K on 5/25/16.
//
//

#import "RSBPickerViewCellItem.h"

@implementation RSBPickerViewCellItem

#pragma mark - RSBPickerViewCellItemProtocol

- (UIView *)viewForRowInPickerView:(UIPickerView *)pickerView reusingView:(UIView *)view {
    UILabel *label = [self viewWithClass:[UILabel class] reusingView:view];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = self.title;
    return label;
}

- (NSString *)titleForRowInPickerView:(UIPickerView *)pickerView {
    return self.title;
}

- (NSAttributedString *)attributedTitleForRowInPickerView:(UIPickerView *)pickerView {
    return nil;
}

- (void)itemDidSelectInPickerView:(UIPickerView *)pickerView atIndexPath:(NSIndexPath *)indexPath {
    if (self.itemDidSelectBlock) {
        self.itemDidSelectBlock(pickerView, indexPath);
    }
}

#pragma mark -

- (id)viewWithClass:(Class)viewClass reusingView:(UIView *)view {
    if (![view isKindOfClass:viewClass]) {
        view = [[viewClass alloc] init];
    }
    return view;
}


@end
