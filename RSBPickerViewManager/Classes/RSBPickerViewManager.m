//
//  RSBPickerViewManager.m
//  Pods
//
//  Created by Anton K on 5/24/16.
//
//

#import "RSBPickerViewManager.h"

@interface RSBPickerViewManager () <UIPickerViewDataSource, RSBPickerViewManagerAdaptorDataSource>

@end

@implementation RSBPickerViewManager

- (id)initWithAdaptor:(id<RSBPickerViewManagerAdaptor>)adaptor {
    if (self = [self initWithPickerView:[[UIPickerView alloc] init] adaptor:adaptor]) {
        //
    }
    return self;
}

- (id)initWithPickerView:(UIPickerView *)pickerView adaptor:(id<RSBPickerViewManagerAdaptor>)adaptor {
    if (self = [super init]) {
        _adaptor = adaptor;
        self.adaptor.dataSource = self;
        
        _pickerView = pickerView;
        self.pickerView.dataSource = self;
        self.pickerView.delegate = self.adaptor;
        [self.pickerView reloadAllComponents];
    }
    return self;
}

- (void)setComponentItems:(NSArray<id<RSBPickerViewComponentItemProtocol>> *)componentItems {
    _componentItems = componentItems;
    [self reloadData];
}

- (void)reloadData {
    [self.pickerView reloadAllComponents];
}

- (void)reloadComponent:(id<RSBPickerViewComponentItemProtocol>)component {
    NSUInteger index = [self.componentItems indexOfObject:component];
    if (index != NSNotFound) {
        [self.pickerView reloadComponent:index];
    }
}

- (void)selectItem:(id<RSBPickerViewCellItemProtocol>)cellItem animated:(BOOL)animated {
    for (id<RSBPickerViewComponentItemProtocol> component in self.componentItems) {
        [self selectItem:cellItem inComponent:component animated:animated];
    }
}

- (void)selectItem:(id<RSBPickerViewCellItemProtocol>)cellItem inComponent:(id<RSBPickerViewComponentItemProtocol>)component animated:(BOOL)animated {
    NSUInteger componentIndex = [self.componentItems indexOfObject:component];
    if (componentIndex == NSNotFound) {
        return;
    }
    
    NSUInteger itemIndex = [[component cellItems] indexOfObject:cellItem];
    if (itemIndex == NSNotFound) {
        return;
    }
    
    [self selectItemAtIndexPath:[NSIndexPath indexPathForRow:itemIndex inSection:componentIndex] animated:animated];
}

- (void)selectItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated {
    [self.pickerView selectRow:indexPath.row inComponent:indexPath.section animated:animated];
}

- (NSArray<id<RSBPickerViewCellItemProtocol>> *)selectedItems {
    NSMutableArray *selectedItems = [NSMutableArray array];
    for (id<RSBPickerViewComponentItemProtocol> component in self.componentItems) {
        id<RSBPickerViewCellItemProtocol> item = [self selectedItemInComponent:component];
        if (item) {
            [selectedItems addObject:item];
        }
    }
    return [selectedItems copy];
}

- (id<RSBPickerViewCellItemProtocol>)selectedItemInComponent:(id<RSBPickerViewComponentItemProtocol>)component {
    NSUInteger componentIndex = [self.componentItems indexOfObject:component];
    if (componentIndex == NSNotFound) {
        return nil;
    }
    
    NSInteger itemIndex = [self.pickerView selectedRowInComponent:componentIndex];
    if (itemIndex < 0) {
        return nil;
    }
    
    return [component cellItems][itemIndex];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return [self.componentItems count];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.componentItems[component].cellItems count];
}

@end
