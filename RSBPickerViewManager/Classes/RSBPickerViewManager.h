//
//  RSBPickerViewManager.h
//  Pods
//
//  Created by Anton K on 5/24/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "RSBPickerViewComponentItemProtocol.h"
#import "RSBPickerViewCellItemProtocol.h"

#import "RSBPickerViewComponentItem.h"
#import "RSBPickerViewCellItem.h"

#import "RSBPickerViewManagerAdaptor.h"

@interface RSBPickerViewManager<__covariant AdaptorType:id<RSBPickerViewManagerAdaptor>> : NSObject

- (id)init NS_UNAVAILABLE;
- (id)initWithAdaptor:(AdaptorType)adaptor;
- (id)initWithPickerView:(UIPickerView *)pickerView adaptor:(AdaptorType)adaptor;

@property (nonatomic, strong, readonly) UIPickerView *pickerView;
@property (nonatomic, strong, readonly) AdaptorType adaptor;

@property (nonatomic, strong) NSArray<id<RSBPickerViewComponentItemProtocol>> *componentItems;

- (void)reloadData;
- (void)reloadComponent:(id<RSBPickerViewComponentItemProtocol>)component;

- (void)selectItem:(id<RSBPickerViewCellItemProtocol>)cellItem animated:(BOOL)animated;
- (void)selectItem:(id<RSBPickerViewCellItemProtocol>)cellItem inComponent:(id<RSBPickerViewComponentItemProtocol>)component animated:(BOOL)animated;
- (void)selectItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated;

- (NSArray<id<RSBPickerViewCellItemProtocol>> *)selectedItems;
- (id<RSBPickerViewCellItemProtocol>)selectedItemInComponent:(id<RSBPickerViewComponentItemProtocol>)component;

@end
