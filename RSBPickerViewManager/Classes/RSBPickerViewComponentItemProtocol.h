//
//  RSBPickerViewComponentItemProtocol.h
//  Pods
//
//  Created by Anton K on 5/24/16.
//
//

#import <Foundation/Foundation.h>

#import "RSBPickerViewCellItemProtocol.h"

@protocol RSBPickerViewComponentItemProtocol <NSObject>

@property (nonatomic, strong) NSArray<id<RSBPickerViewCellItemProtocol>> *cellItems;

- (CGFloat)widthForComponentInPickerView:(UIPickerView *)pickerView;
- (CGFloat)heightForRowInPickerView:(UIPickerView *)pickerView;

@end
