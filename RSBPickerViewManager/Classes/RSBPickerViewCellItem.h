//
//  RSBPickerViewCellItem.h
//  Pods
//
//  Created by Anton K on 5/25/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "RSBPickerViewCellItemProtocol.h"

@interface RSBPickerViewCellItem : NSObject <RSBPickerViewCellItemProtocol>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, copy) void(^itemDidSelectBlock)(__weak UIPickerView *pickerView, NSIndexPath *indexPath);

- (id)viewWithClass:(Class)viewClass reusingView:(UIView *)view;

@end
