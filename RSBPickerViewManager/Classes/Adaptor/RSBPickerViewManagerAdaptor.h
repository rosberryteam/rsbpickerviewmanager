//
//  RSBPickerViewManagerAdaptor.h
//  Pods
//
//  Created by Anton K on 7/26/17.
//
//

#import <UIKit/UIKit.h>
@protocol RSBPickerViewComponentItemProtocol;

@protocol RSBPickerViewManagerAdaptorDataSource <NSObject>

- (NSArray<id<RSBPickerViewComponentItemProtocol>> *)componentItems;

@end

@protocol RSBPickerViewManagerAdaptor <UIPickerViewDelegate>

@property (nonatomic) id<RSBPickerViewManagerAdaptorDataSource> dataSource;

@end
