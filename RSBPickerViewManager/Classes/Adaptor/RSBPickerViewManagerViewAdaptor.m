//
//  RSBPickerViewManagerViewAdaptor.m
//  Pods
//
//  Created by Anton K on 7/26/17.
//
//

#import "RSBPickerViewManagerViewAdaptor.h"

#import "RSBPickerViewComponentItemProtocol.h"
#import "RSBPickerViewCellItemProtocol.h"

@implementation RSBPickerViewManagerViewAdaptor

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    return [self.dataSource.componentItems[component].cellItems[row] viewForRowInPickerView:pickerView reusingView:view];
}

@end
