//
//  RSBPickerViewManagerBaseAdaptor.h
//  Pods
//
//  Created by Anton K on 7/26/17.
//
//

#import <Foundation/Foundation.h>
#import "RSBPickerViewManagerAdaptor.h"

@interface RSBPickerViewManagerBaseAdaptor : NSObject <RSBPickerViewManagerAdaptor>

@end
