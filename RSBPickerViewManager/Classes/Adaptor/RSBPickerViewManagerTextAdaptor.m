//
//  RSBPickerViewManagerTextAdaptor.m
//  Pods
//
//  Created by Anton K on 7/26/17.
//
//

#import "RSBPickerViewManagerTextAdaptor.h"

#import "RSBPickerViewComponentItemProtocol.h"
#import "RSBPickerViewCellItemProtocol.h"

@implementation RSBPickerViewManagerTextAdaptor

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.dataSource.componentItems[component].cellItems[row] titleForRowInPickerView:pickerView];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    id<RSBPickerViewCellItemProtocol> item = self.dataSource.componentItems[component].cellItems[row];
    if (![item respondsToSelector:@selector(attributedTitleForRowInPickerView:)]) {
        return nil;
    }
    
    return [item attributedTitleForRowInPickerView:pickerView];
}

@end
