//
//  RSBPickerViewManagerBaseAdaptor.m
//  Pods
//
//  Created by Anton K on 7/26/17.
//
//

#import "RSBPickerViewManagerBaseAdaptor.h"

#import "RSBPickerViewComponentItemProtocol.h"
#import "RSBPickerViewCellItemProtocol.h"

@implementation RSBPickerViewManagerBaseAdaptor

@synthesize dataSource;

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    CGFloat width = [self.dataSource.componentItems[component] widthForComponentInPickerView:pickerView];
    if (width == 0.0) {
        width = pickerView.bounds.size.width / [self.dataSource.componentItems count];
    }
    
    return width;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    if (component >= [self.dataSource.componentItems count]) {
        return 0.0;
    }
    
    return [self.dataSource.componentItems[component] heightForRowInPickerView:pickerView];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self.dataSource.componentItems[component].cellItems[row] itemDidSelectInPickerView:pickerView atIndexPath:[NSIndexPath indexPathForRow:row inSection:component]];
}

@end
