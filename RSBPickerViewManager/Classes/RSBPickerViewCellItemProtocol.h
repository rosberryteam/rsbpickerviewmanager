//
//  RSBPickerViewCellItemProtocol.h
//  Pods
//
//  Created by Anton K on 5/24/16.
//
//

#import <Foundation/Foundation.h>

@protocol RSBPickerViewCellItemProtocol <NSObject>

- (UIView *)viewForRowInPickerView:(UIPickerView *)pickerView reusingView:(UIView *)view;
- (NSString *)titleForRowInPickerView:(UIPickerView *)pickerView;

- (void)itemDidSelectInPickerView:(UIPickerView *)pickerView atIndexPath:(NSIndexPath *)indexPath;

@optional
- (NSAttributedString *)attributedTitleForRowInPickerView:(UIPickerView *)pickerView;

@end
