//
//  RSBPickerViewComponentItem.h
//  Pods
//
//  Created by Anton K on 5/25/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "RSBPickerViewComponentItemProtocol.h"
#import "RSBPickerViewCellItemProtocol.h"

#define RSBPickerViewComponentWidthAutomatic 0.0

@interface RSBPickerViewComponentItem : NSObject <RSBPickerViewComponentItemProtocol>

- (id)initWithItems:(NSArray<id<RSBPickerViewCellItemProtocol>> *)items;

@property (nonatomic, assign) CGFloat componentWidth;
@property (nonatomic, assign) CGFloat rowHeight;

@end
