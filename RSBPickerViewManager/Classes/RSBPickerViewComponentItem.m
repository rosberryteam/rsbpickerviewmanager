//
//  RSBPickerViewComponentItem.m
//  Pods
//
//  Created by Anton K on 5/25/16.
//
//

#import "RSBPickerViewComponentItem.h"

@implementation RSBPickerViewComponentItem

@synthesize cellItems;

- (id)initWithItems:(NSArray<id<RSBPickerViewCellItemProtocol>> *)items {
    if (self = [super init]) {
        self.cellItems = items;
        
        self.componentWidth = RSBPickerViewComponentWidthAutomatic;
        self.rowHeight = 44.0;
    }
    return self;
}

- (CGFloat)widthForComponentInPickerView:(UIPickerView *)pickerView {
    return self.componentWidth;
}

- (CGFloat)heightForRowInPickerView:(UIPickerView *)pickerView {
    return self.rowHeight;
}

@end
